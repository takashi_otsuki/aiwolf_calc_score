#!/usr/bin/env python
'''
Created on 2016/04/25

@author: otsuki
'''

import argparse
import glob

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--games', required=True, type=int)
    parser.add_argument('--logs', required=True)
    parser.add_argument('--csv', action='store_true')
    args = parser.parse_args()
    ngame = 0
    agent = {}
    for f in glob.glob(args.logs):
        rolemap = {}
        for line in open(f, 'r'):
            data = line.split(',')
            if data[0] == '0' and data[1] == 'status':
                role = data[3]
                name = data[5].rstrip()
                rolemap[name] = role
                if not agent.has_key(name):
                    agent[name] = {}
                    for r in ['VILLAGER', 'SEER', 'MEDIUM', 'BODYGUARD', 'POSSESSED', 'WEREWOLF']:
                        agent[name][r] = dict(app=0, win=0)
                agent[name][role]['app'] += 1
            elif data[1] == 'result':
                ngame += 1
                winner = data[4].rstrip()
                if winner == 'VILLAGER':
                    for name in agent.keys():
                        role = rolemap[name]
                        if role == 'BODYGUARD' or role == 'MEDIUM' or role == 'SEER' or role == 'VILLAGER':
                            agent[name][role]['win'] += 1
                elif winner == 'WEREWOLF':
                    for name in agent.keys():
                        role = rolemap[name]
                        if role == 'POSSESSED' or role == 'WEREWOLF':
                            agent[name][role]['win'] += 1
        if ngame == args.games: break
                            
    # print the result
    if not args.csv:
        print str(ngame).ljust(20),
        for role in ['VILLAGER', 'SEER', 'MEDIUM', 'BODYGUARD', 'POSSESSED', 'WEREWOLF']:
            print role.center(17),
        print 'TOTAL'.center(7)
        for name in agent.keys():
            print name.ljust(20)[:20],
            nwin = 0
            for role in ['VILLAGER', 'SEER', 'MEDIUM', 'BODYGUARD', 'POSSESSED', 'WEREWOLF']:
                print str(agent[name][role]['win']).rjust(7) + ' / ' + str(agent[name][role]['app']).ljust(7),
                nwin += agent[name][role]['win']
            print str(nwin).center(7)
    else:
        print 'NAME,VILLAGER,,,SEER,,,MEDIUM,,,BODYGUARD,,,POSSESSED,,,WEREWOLF,,,OVERALL,,'
        for name in agent.keys():
            print name + ',',
            nwin = 0
            for role in ['VILLAGER', 'SEER', 'MEDIUM', 'BODYGUARD', 'POSSESSED', 'WEREWOLF']:
                print '%f,%d,%d,' % (float(agent[name][role]['win']) / agent[name][role]['app'], agent[name][role]['win'], agent[name][role]['app']),
                nwin += agent[name][role]['win']
            print '%f,%d,%d' % (float(nwin) / ngame, nwin, ngame)
        
