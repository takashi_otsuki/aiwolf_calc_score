# calc_score.py

This tool calculates AIWolf game score from game logs.

## Usage

` calc_score.py --games numGames --logs logFiles [--csv] `

Calculate score from `numGames` game logs in the specified log files
and write the result to the standard output.
If the number of specified log files is less than `numGames`,
the former is used.
The result is in CSV format when `--csv` option is given.
You can specify multiple log files using regular expression in `logFiles` argument.

## Example

` calc_score.py --games 1000 --logs log/*.log --csv `

Calculate the score from 1000 log files in `log` folder
and output the result in CSV format to the standard output.